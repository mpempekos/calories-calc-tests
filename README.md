# calories-calc-tests

The only requirement for this repository is docker.
In order to trigger the entire test suite: 
`./run_tests.sh`

In order to trigger specific tests, provide the tags of the 
test cases as arguments, separated by comma, eg:
`./run_tests.sh register-wrong-pass-confirmation,register-short-pass`

If a test fails, a screenshot is taken and stored in current directory.
For running only specific tests, 'register-success' test case is
required, so that we login to the application. 
In a real scenario, an insertion to database before the beginning of the tests would be ideal.
