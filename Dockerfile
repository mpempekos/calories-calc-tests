FROM python:2.7-stretch

RUN set -x && \
    apt-get update -yq && \
    apt-get -yq --no-install-recommends install \
    ca-certificates \
    curl \
    unzip \
    libgconf-2-4 \
    libav-tools \
    vim \
    jq \
    less \
    socat \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' && \
    apt-get update -y && \
    apt-get -y install google-chrome-stable && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/*

ARG CHROMEDRIVER_VERSION=2.43
RUN curl -SLO "https://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip" && \
    unzip chromedriver_linux64.zip && \
    mv chromedriver /usr/local/bin && \
    rm chromedriver_linux64.zip

COPY container/requirements.txt /requirements.txt

RUN pip install --no-cache-dir -r  /requirements.txt

COPY . /tests/
WORKDIR /tests/
