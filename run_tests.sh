#!/bin/bash

command_exists() {
    command -v $@ > /dev/null 2>&1
}

# verify that docker exists
if ! command_exists docker; then
    echo "Docker is not installed. Aborting."
fi

# build image if doesn't exist locally
if [[ ! $(docker images | grep calories-calc-tests) ]]; then
    echo "Image calories-calc-tests not found. Building it..."
    docker build -t calories-calc-tests .
fi

if [ "$@" == "" ]; then
    command="behave -k --no-capture --no-capture-stderr ui/features"
else
    command="behave -k --no-capture --no-capture-stderr --tags="$@" ui/features"
fi

docker run --rm -it -v $(pwd):/tests/artifacts --shm-size=1g \
      -- calories-calc-tests $command
