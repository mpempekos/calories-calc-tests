import os
import random


def get_setting(setting, default_value=None):
    '''Returns environmental variable if set, default value otherwise.'''
    return os.environ.get(setting) or default_value


CALORIES_CALC_URL = get_setting("CALORIES_CALC_URL",
                                "http://calories-calc.herokuapp.com")

RANDOM_EMAIL = get_setting("RANDOM_EMAIL", "tester%d@test.com" % (random.randint(1, 2000000)))

BROWSER = get_setting("BROWSER", "chrome")

# CHROME SETTINGS
CHROME_PATH = get_setting("CHROME_PATH", "/usr/bin/google-chrome")
CHROME_OPTIONS = get_setting('WEBDRIVER_OPTIONS',
                             ['headless', 'no-sandbox', 'disable-gpu',
                              'window-size=1920x1080'])

ARTIFACTS_PATH = get_setting("ARTIFACTS_PATH", "artifacts")
