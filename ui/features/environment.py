import logging
import os
import json

import config

from selenium import webdriver

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def before_all(context):
    log.info('Starting before_all hook...')
    log.info('Initializing driver...')
    context.browser = get_driver(config.BROWSER)
    context.config = {}
    context.config['RANDOM_EMAIL'] = config.RANDOM_EMAIL
    context.config['CALORIES_CALC_URL'] = config.CALORIES_CALC_URL
    context.config['ARTIFACTS_PATH'] = config.ARTIFACTS_PATH
    log.info('Finished with before_all hook. Starting tests...')


def get_driver(browser='chrome'):
    """
    Returns an instance of a remote selenium driver
    """
    if browser == "firefox":
        driver = webdriver.Firefox()
    elif browser == "chrome":
        options = webdriver.ChromeOptions()
        options.binary_location = config.CHROME_PATH

        for opt in config.CHROME_OPTIONS:
            options.add_argument(opt)
        driver = webdriver.Chrome(chrome_options=options)
    else:
        raise Exception("%s is not supported!" % browser)

    return driver


def after_step(context, step):
    if step.status == "failed":
        try:
            get_error_screenshot(context, step)
        except Exception as e:
            log.error("Could not get screen shot: %s" % repr(e))


def get_error_screenshot(context, step):
    path = context.config['ARTIFACTS_PATH'] + '/error.png'
    try:
        context.browser.save_screenshot(path)
    except Exception:
        pass
