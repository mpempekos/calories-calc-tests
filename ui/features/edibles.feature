@edibles
Feature: Actions related to edibles, such as add, edit, delete etc

   @set-max-calories
   Scenario: User successfully sets max daily calories from profile section. Value is properly update on edibles section.
      When I click the "Profile" option from navigation menu
      And I type "1000" in the "Max Daily Calories" input
      And I click the "Edibles" option from navigation menu
      And I wait for 1 seconds
      Then max daily calories should be "1000"
      And calorybox should be "green"

   @add-edible-success
   Scenario: Add successfully an edible. Daily calories are updated.
      When I click the "ADD" button
      And I type "Pizza" in the "Name" input
      And I type "800" in the "Calories" input
      And I click the "Save" button
      And I wait for 1 seconds
      Then used daily calories should be "800"
      And calorybox should be "green"
      And "Pizza" with "800" calories should be "present" in the edibles list

   @set-max-calories-smaller-than-daily
   Scenario: Set max calories smaller than current daily calories. Calories in edibles section are written in a red background.
      When I click the "Profile" option from navigation menu
      And I type "500" in the "Max Daily Calories" input
      And I click the "Edibles" option from navigation menu
      And I wait for 1 seconds
      Then max daily calories should be "500"
      And calorybox should be "red"

   @add-edible-empty-calories
   Scenario: User tried to add an edible without specifying name. Addition fails, daily calories are not updated. Error message is shown.
      When I click the "ADD" button
      And I type "800" in the "Calories" input
      And I click the "Save" button
      Then used daily calories should be "800"
      And I should see the error "Unprocessable entity"

   @edit-edible-success
   Scenario: Edit successfully an edible. Daily calories and calories information background colour are updated.
      When I "edit" "Pizza" with "800" calories
      And I type "400" in the "Calories" input
      And I click the "Save" button
      Then used daily calories should be "400"
      And calorybox should be "green"
      And "Pizza" with "400" calories should be "present" in the edibles list

   @edit-edible-fail
   Scenario: User tried to edit an edible, leaving calories input empty. Edit fails, daily calories are not updated. Error message is shown.
      When I "edit" "Pizza" with "400" calories
      And I type " " in the "Calories" input
      And I click the "Save" button
      Then used daily calories should be "400"
      And I should see the error "Unprocessable entity"

   @filter-by-date
   Scenario: User filters by setting a past date as end date. Existing edible is absent from edibles list
      When I click the "FILTER BY DATE & TIME INTERVAL" button
      And I wait for 1 seconds
      And I type "12/12/2018" in the "endDate" input
      And I click the "Filter" button
      And I wait for 1 seconds
      Then "Pizza" with "400" calories should be "absent" in the edibles list

   @reset-filter-by-date
   Scenario: User resets the filter. Existing edible becomes present in edibles list
      When I click the "Reset filter" button
      And I wait for 1 seconds
      Then "Pizza" with "400" calories should be "present" in the edibles list

   @delete-edible-success
   Scenario: Delete successfully an edible. Daily calories are updated and edible is absent from edibles list
      When I "delete" "Pizza" with "400" calories
      Then used daily calories should be "0"
      And "Pizza" with "400" calories should be "absent" in the edibles list
