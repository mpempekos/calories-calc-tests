from behave import step

from time import sleep

from selenium.common.exceptions import NoSuchElementException


@step(u'I should see the error "{error_msg}"')
def see_error_message(context, error_msg):
    error_msg = error_msg.lower()
    try:
        error = context.browser.find_element_by_class_name('flash')
    except NoSuchElementException:
        sleep(1)
        error = context.browser.find_element_by_class_name('flash')
    assert error.text.lower() == error_msg, "Error message is not %s, it is %s" % (error_msg, error.text)
