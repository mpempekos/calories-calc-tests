from behave import step

from time import sleep

from selenium.common.exceptions import NoSuchElementException
from utils import click_item_from_list


@step(u'I click the button "{button_text}" to submit my credentials')
def click_join_button(context, button_text):
    if button_text.lower() not in ['join', 'login']:
        raise Exception('Unknown button')
    btn = context.browser.find_element_by_xpath("//input[@type='submit']")
    btn.click()


@step(u'I click the "{button_text}" button')
def click_button(context, button_text):
    buttons = context.browser.find_elements_by_tag_name('button')
    assert buttons, "Couldn't find any buttons"
    click_item_from_list(button_text, buttons)
