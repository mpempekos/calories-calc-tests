from behave import step

from time import sleep

from selenium.common.exceptions import NoSuchElementException
from utils import click_item_from_list


@step('max daily calories should be "{max_calories}"')
def check_max_calories(context, max_calories):
    try:
        calory_box = context.browser.find_element_by_class_name('calorybox')
    except NoSuchElementException:
        sleep(1)
        calory_box = context.browser.find_element_by_class_name('calorybox')
    max_cals = calory_box.text.split('/')[1].split(' CALS')[0]
    assert max_cals == max_calories, "Existing max calories are not %s, they are %s" % (max_calories, max_cals)


@step('used daily calories should be "{daily_calories}"')
def check_daily_calories(context, daily_calories):
    try:
        calory_box = context.browser.find_element_by_class_name('calorybox')
    except NoSuchElementException:
        sleep(1)
        calory_box = context.browser.find_element_by_class_name('calorybox')
    daily_cals = calory_box.text.split('/')[0].split('TODAY ')[1]
    assert daily_cals == daily_calories, "Existing daily calories are not %s, they are %s" % (daily_calories, daily_cals)


def get_edible(context, name, calories):
    edibles = context.browser.find_elements_by_tag_name('tr')
    for edible in edibles:
        if ((name + ' ' + calories) in edible.text):
            return edible
    return None


@step('"{name}" with "{calories}" calories should be "{state}" in the edibles list')
def is_edible_present(context, name, calories, state):
    if state not in ['present', 'absent']:
        raise Exception('Unknown state')

    edible = get_edible(context, name, calories)

    if state == 'present' and edible:
        return
    if state == 'absent' and not edible:
        return

    assert False, "%s with %s calories was not %s in the edibles list" % (name, calories, state)


@step('I "{action}" "{name}" with "{calories}" calories')
def edit_edible(context, action, name, calories):
    if action not in ['edit', 'delete']:
        raise Exception('Unknown action')
    edibles = context.browser.find_elements_by_tag_name('tr')
    for edible in edibles:
        if ((name + ' ' + calories) in edible.text):
            options = edible.find_elements_by_tag_name('button')
            click_item_from_list(action, options)
            return
    assert False, "%s with %s calories was not found in the edibles list" % (name, calories)


@step('calorybox should be "{colour}"')
def check_calorybox_colour(context, colour):
    if colour.lower() not in ['green', 'red']:
        raise Exception('Unknown colour')
    calory_box = context.browser.find_element_by_class_name('calorybox')
    calory_box_classes = calory_box.get_attribute('class')
    assert colour in calory_box_classes, "Calory box should be %s, but it is not." % colour
