from behave import step

from time import sleep


@step('I wait for {seconds} seconds')
def wait_for_some_seconds(context, seconds):
    sleep(int(seconds))


def click_item_from_list(button_text, items):
    for item in items:
        if item.text.lower() == button_text.lower():
            item.click()
            return
    assert False, "Could not find button with text %s" % button_text
