from behave import step


@step(u'I type "{text}" in the "{input_name}" input')
def write_text_to_input(context, text, input_name):
    '''Tries to find name attribute of element based on the description of the field'''
    if context.config.get(text):
        text = context.config.get(text)
    if input_name not in ['endDate']:
    	input_name = input_name.replace(' ', '_').lower()
    input = context.browser.find_element_by_xpath("//input[@name='{}']".format(input_name))
    input.clear()
    input.send_keys(text)
