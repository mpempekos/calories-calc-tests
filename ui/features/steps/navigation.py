from behave import step

from selenium.common.exceptions import NoSuchElementException


@step(u'I click the "{menu_option}" option from navigation menu')
def select_nav_menu_option(context, menu_option):
    options = context.browser.find_elements_by_tag_name('li')
    for option in options:
        if option.text.lower() == menu_option.lower():
            try:
                option.find_element_by_tag_name('a').click()
            except NoSuchElementException:
                option.click()
            return


@step('aside menu should be "{visibility}"')
def is_aside_menu_visible(context, visibility):
    if visibility not in ['visible', 'invisible']:
        raise Exception('Unknown type of visibility')
    try:
        context.browser.find_element_by_tag_name('aside')
        assert visibility == 'visible', "Menu is visible, but should be invisible"
    except NoSuchElementException:
        assert visibility == 'invisible', "Menu is invisible, but should be visible"
