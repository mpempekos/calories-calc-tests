from behave import step

from time import sleep


@step('I visit "{url}"')
def visit_url(context, url):
    if 'CALORIES_CALC_URL' in url:
        url = url.replace('CALORIES_CALC_URL',
                          context.config.get('CALORIES_CALC_URL'))
    context.browser.get(url)
    sleep(1)


@step('current url should be "{url}"')
def get_current_url(context, url):
    if url.startswith('CALORIES_CALC_URL'):
        url = url.replace('CALORIES_CALC_URL',
                          context.config.get('CALORIES_CALC_URL'))
    current_url = context.browser.current_url
    assert url == current_url, "Current url is not %s, it is \
                                %s" % (url, current_url)
