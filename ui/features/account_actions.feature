@user-actions
Feature: Actions related to user, such as register, login, logout

   @register-wrong-pass-confirmation
   Scenario: User tries to register but gives wrong password confirmation. Registration should fail.
      When I visit "CALORIES_CALC_URL"
      And I click the "Register" button
      And I wait for 1 seconds
      And I type "John" in the "Name" input
      And I type "RANDOM_EMAIL" in the "Email" input
      And I type "p@ssW0RD" in the "Password" input
      And I type "p@ssW0RDsd" in the "Password confirmation" input
      And I click the button "Join" to submit my credentials
      Then I should see the error "Registration failed. Please try again"
      And aside menu should be "invisible"

   @register-short-pass
   Scenario: User tries to register but gives password shorter than 8 characters. Registration should fail.
      When I visit "CALORIES_CALC_URL"
      And I click the "Register" button
      And I wait for 1 seconds
      And I type "John" in the "Name" input
      And I type "RANDOM_EMAIL" in the "Email" input
      And I type "p@ss" in the "Password" input
      And I type "p@ss" in the "Password confirmation" input
      And I click the button "Join" to submit my credentials
      Then I should see the error "Registration failed. Please try again"
      And aside menu should be "invisible"

   @register-success
   Scenario: User successfully completes registration, then logs out.
      When I visit "CALORIES_CALC_URL"
      And I click the "Register" button
      And I wait for 1 seconds
      Then current url should be "CALORIES_CALC_URL/register"
      And I type "John" in the "Name" input
      And I type "RANDOM_EMAIL" in the "Email" input
      And I type "p@ssW0RD" in the "Password" input
      And I type "p@ssW0RD" in the "Password confirmation" input
      And I click the button "Join" to submit my credentials
      And I wait for 1 seconds
      Then aside menu should be "visible"
      And current url should be "CALORIES_CALC_URL/edibles"
      When I click the "Logout" option from navigation menu
      And I wait for 1 seconds
      Then aside menu should be "invisible"
      And current url should be "CALORIES_CALC_URL/"

   @login
   Scenario: User logs in successfully.
      When I visit "CALORIES_CALC_URL"
      And I click the "Login" button
      And I wait for 1 seconds
      And I type "RANDOM_EMAIL" in the "Email" input
      And I type "p@ssW0RD" in the "Password" input
      And I click the button "Login" to submit my credentials
      And I wait for 1 seconds
      Then aside menu should be "visible"
      And current url should be "CALORIES_CALC_URL/edibles"
      And max daily calories should be "3000"
      And used daily calories should be "0"
      And calorybox should be "green"

   @logout-login
   Scenario: User who is logged in, logs out. Then he logs in again
      When I click the "Logout" option from navigation menu
      And I wait for 1 seconds
      Then aside menu should be "invisible"
      And current url should be "CALORIES_CALC_URL/"
      And I click the "Login" button
      And I wait for 1 seconds
      And I type "RANDOM_EMAIL" in the "Email" input
      And I type "p@ssW0RD" in the "Password" input
      And I click the button "Login" to submit my credentials
      And I wait for 1 seconds
      Then aside menu should be "visible"
      And current url should be "CALORIES_CALC_URL/edibles"
